//
//  CSVFilter.cpp
//
//  Created by Saud Ahmed on 7/25/17.
//
//

#include "Sort.h"
#include "DummyClasses.h"

/***** Static variables *****/

Sort* Sort::instance = nullptr;

/***** End static variables *****/

/***** Constructors and destructor. *****/

Sort::Sort(){

}

Sort::~Sort(){

}

/***** End constructors and destructor. *****/

/***** Static methods. *****/

Sort* Sort::getInstance(){
    if (Sort::instance == nullptr) {
        Sort::instance = new Sort();
    }
    
    return Sort::instance;
}

/***** End static methods. *****/

/***** Instance methods. *****/
void Sort::sortContainer1ByContainer2(std::vector<Object*> container1, std::vector<int> container2, bool ascend)
{
    int container1Size = static_cast<int>(container1.size());
    int container2Size = static_cast<int>(container2.size());
    
    if (container1Size == container2Size)
    {
        this->sortingAlgorithm(container1, container2, ascend);
    }
    else
    {
        printf("\nSize not same of both containers: sorting not possible\n");
    }
}

void Sort::sortingAlgorithm(std::vector<Object*> container1, std::vector<int> container2, bool ascend)
{
    int c, d, swap, size;
    
    size = static_cast<int>(container1.size());
    
    if (ascend)
    {
        for (c = 0 ; c < ( size - 1 ); c++)
        {
            for (d = 0 ; d < size - c - 1; d++)
            {
                if (container2.at(d) > container2.at(d+1)) /* For ascending order */
                {
                    swap       = container2.at(d);
                    container2.at(d)   = container2.at(d+1);
                    container2.at(d+1) = swap;
                    
                    Object * swapObject = container1.at(d);
                    container1.at(d) = container1.at(d + 1);
                    container1.at(d + 1) = swapObject;
                }
            }
        }
        
        printf("Sorted list in ascending order:\n");
    }
    else
    {
        for (c = 0 ; c < ( size - 1 ); c++)
        {
            for (d = 0 ; d < size - c - 1; d++)
            {
                if (container2.at(d) < container2.at(d+1)) /* For ascending order */
                {
                    swap       = container2.at(d);
                    container2.at(d)   = container2.at(d+1);
                    container2.at(d+1) = swap;
                    
                    Object *swapObject = container1.at(d);
                    container1.at(d) = container1.at(d + 1);
                    container1.at(d + 1) = swapObject;
                }
            }
        }
        
        printf("Sorted list in descending order:\n");
    }

    this->printContainer1Details(container1);
    this->printContainer2Details(container2);
}

void Sort::printContainer1Details(std::vector<Object*> container1)
{
    int size = static_cast<int>(container1.size());
    
    printf("Container 1 : [ ");
    
    for ( int j = 0 ; j < size ; j++ )
    {
        Object *object = container1.at(j);
        
        if (dynamic_cast<DerivedObject<int> *> (object))
        {
            DerivedObject<int> * objectInt = dynamic_cast<DerivedObject<int> *> (object);
            
            int element = objectInt->getElement();
            
            printf("%i ", element);
        }
        else if (dynamic_cast<DerivedObject<std::string> *> (object))
        {
            DerivedObject<string> * objectString = dynamic_cast<DerivedObject<string> *> (object);
            
            string element = objectString->getElement();
            
            printf("%s ", element.c_str());
        }
        else if (dynamic_cast<DerivedObject<float> *> (object))
        {
            DerivedObject<float> * objectFloat = dynamic_cast<DerivedObject<float> *> (object);
            
            float element = objectFloat->getElement();
            
            printf("%f ", element);
        }
        else if (dynamic_cast<DerivedObject<long> *> (object))
        {
            DerivedObject<long> * objectLong = dynamic_cast<DerivedObject<long> *> (object);
            
            long element = objectLong->getElement();
            
            printf("%ld", element);
        }
        else if (dynamic_cast<DerivedObject<double> *> (object))
        {
            DerivedObject<double> * objectDouble = dynamic_cast<DerivedObject<double> *> (object);
            
            double element = objectDouble->getElement();
            
            printf("%f ", element);
        }
        else if (dynamic_cast<DerivedObject<Person*> *> (object))
        {
            DerivedObject<Person*> * objectPerson = dynamic_cast<DerivedObject<Person*> *> (object);
            
            Person* person = objectPerson->getElement();
            
            std::string name = person->getName();
            
            printf("%s ", name.c_str());
        }
        else if (dynamic_cast<DerivedObject<Animal*> *> (object))
        {
            DerivedObject<Animal*> * objectAnimal = dynamic_cast<DerivedObject<Animal*> *> (object);
            
            Animal* animal = objectAnimal->getElement();
            
            std::string name = animal->getName();
            
            printf("%s ", name.c_str());
        }
        else if (dynamic_cast<DerivedObject<Thing*> *> (object))
        {
            DerivedObject<Thing*> * objectThing = dynamic_cast<DerivedObject<Thing*> *> (object);
            
            Thing* thing = objectThing->getElement();
            
            std::string name = thing->getName();
            
            printf("%s ", name.c_str());
        }
    }
    
    printf("]");
    
    printf("\n");
    

}

void Sort::printContainer2Details(std::vector<int> container2)
{
    int size = static_cast<int>(container2.size());
    
    printf("Container 2 : [ ");
    
    for ( int c = 0 ; c < size ; c++ )
        printf("%d ", container2[c]);
    
    printf("]");
    
    printf("\n");

}
/***** End Instance methods. *****/
