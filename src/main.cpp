#include<iostream>

#include "Sort.h"
#include "DummyClasses.h"

int main(int argc, char *argv[]){

    std::vector<Object*> objectsArray;
    
    DerivedObject<int> * object1 = new DerivedObject<int>(1);
    
    DerivedObject<string> * object2 = new DerivedObject<std::string>("apple");
    
    DerivedObject<float> * object3 = new DerivedObject<float>(1.0f);
    
    Person * person = new Person("George");
    DerivedObject<Person*> * object4 = new DerivedObject<Person*>(person);
    
    Animal * animal = new Animal("Cat");
    DerivedObject<Animal*> * object5 = new DerivedObject<Animal*>(animal);
    
    Thing * thing = new Thing("Table");
    DerivedObject<Thing*> * object6 = new DerivedObject<Thing*>(thing);
    
    objectsArray.push_back(object1);
    objectsArray.push_back(object2);
    objectsArray.push_back(object3);
    objectsArray.push_back(object4);
    objectsArray.push_back(object5);
    objectsArray.push_back(object6);
    
    std::vector<int> container2;
    
    container2.push_back(4);
    container2.push_back(7);
    container2.push_back(1);
    container2.push_back(5);
    container2.push_back(9);
    container2.push_back(3);
    
    Sort::getInstance()->printContainer1Details(objectsArray);
    Sort::getInstance()->printContainer2Details(container2);
    
    printf("\n\n");
    
    Sort::getInstance()->sortContainer1ByContainer2(objectsArray, container2, true);
    printf("\n\n");
    Sort::getInstance()->sortContainer1ByContainer2(objectsArray, container2, false);
    
    CC_SAFE_DELETE(object1);
    CC_SAFE_DELETE(object2);
    CC_SAFE_DELETE(object3);
    CC_SAFE_DELETE(object4);
    CC_SAFE_DELETE(object5);
    CC_SAFE_DELETE(object6);
    
    return 0;
}
