//
//  DerivedObject.h
//
//  Created by Saud Ahmed on 7/26/17.
//
//

#ifndef DerivedObject_h
#define DerivedObject_h

#include <stdio.h>
#include "Object.h"
#include "Macros.h"

template< class T >
class DerivedObject
: public Object
{
public:
    DerivedObject(T element): element_(element) {}
    virtual ~DerivedObject(){};
    
    CC_SYNTHESIZE(T, element_, Element);
};

#endif /* DerivedObject_h */
