//
//  Object.h
//
//  Created by Saud Ahmed on 7/25/17.
//
//

#ifndef Object_h
#define Object_h

#include <stdio.h>

class Object
{
public:
    Object(){};
    virtual ~Object() {};
};

#endif /* Object_h */
