//
//  Sort.h
//
//  Created by Saud Ahmed on 7/25/17.
//
//

#ifndef Sort_h
#define Sort_h

#include <stdio.h>
#include <iostream>
#include <vector>

#include "DerivedObject.h"
#include "Macros.h"


using namespace std;

class Sort
{
private:
    Sort();
    virtual ~Sort();
    
    // Static variables.
    static Sort* instance;
    
    //Instance variables.
    
    //Instance Methods
    void sortingAlgorithm(std::vector<Object*> container1, std::vector<int> container2, bool ascend);
    
public:
    
    // Static methods.
    static Sort* getInstance();
    
    //Instance Methods
    void sortContainer1ByContainer2(std::vector<Object*> container1, std::vector<int> container2, bool ascend);

    void printContainer1Details(std::vector<Object*> container1);
    void printContainer2Details(std::vector<int> container2);
};
#endif /* Sort_h */
