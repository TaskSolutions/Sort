//
//  DummyClasses.h
//
//  Created by Saud Ahmed on 7/26/17.
//
//

#ifndef DummyClasses_hpp
#define DummyClasses_hpp

#include <stdio.h>
#include "Macros.h"

class Person {
public:
    Person(std::string name): name_(name){}
    ~Person(){};
    CC_SYNTHESIZE(std::string, name_, Name);
};

class Animal {
public:
    Animal(std::string name): name_(name){}
    ~Animal(){};
    CC_SYNTHESIZE(std::string, name_, Name);
};

class Thing {
public:
    Thing(std::string name): name_(name){}
    ~Thing(){};
    CC_SYNTHESIZE(std::string, name_, Name);
};

#endif /* DummyClasses_h */
